package com.texoit.teste.parse;

public interface Parse<D, P> {

	public P parse(D de); 
	
}
