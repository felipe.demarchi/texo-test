package com.texoit.teste.model.projection;

public interface MultiProducersInfoProjection {

	Long getProducerId();
	
	int getYear();
	
	String getProducerName();
	
}
