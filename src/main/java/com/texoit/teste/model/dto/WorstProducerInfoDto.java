package com.texoit.teste.model.dto;

import java.io.Serializable;

public class WorstProducerInfoDto implements Serializable {

	private static final long serialVersionUID = 4024610904202813396L;
	
	private String producer;
	
	private int interval;
	
	private int previousWin;
	
	private int followingWin;

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public int getPreviousWin() {
		return previousWin;
	}

	public void setPreviousWin(int previousWin) {
		this.previousWin = previousWin;
	}

	public int getFollowingWin() {
		return followingWin;
	}

	public void setFollowingWin(int followingWin) {
		this.followingWin = followingWin;
	}

}
