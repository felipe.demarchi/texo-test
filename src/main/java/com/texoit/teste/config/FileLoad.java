package com.texoit.teste.config;

import java.io.FileReader;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.opencsv.bean.CsvToBeanBuilder;
import com.texoit.teste.model.dto.CsvMovieDto;
import com.texoit.teste.service.api.MovieService;

@Component
public class FileLoad implements ApplicationRunner {

	@Value("${csv.movielist}")
	private String csvPath;
	
	@Autowired
	private MovieService movieService;
		
	@Override
	public void run(ApplicationArguments args) throws Exception {
		FileReader fileReader = new FileReader(this.csvPath);
		List<CsvMovieDto> moviesDto = new CsvToBeanBuilder(fileReader)
				.withType(CsvMovieDto.class)
				.withSeparator(';')
				.build()
				.parse();
		moviesDto.forEach(movieDto -> this.movieService.save(movieDto));
	}

}
