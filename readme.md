<h1 style="text-align: center">Teste - Golden Raspberry Awards</h1>
<h3 style="margin-top: 30px;">Requisitos</h3>
<ul>
<li>Java (OpenJDK 17.0.5)</li>
<li>Maven 3.8.6</li>
</ul>
<h3 style="margin-top: 30px;">Executando a aplicação</h3>
<h5>Opção 1</h5>
<p>Na IDE de desenvolvimento, importar o projeto como <b>Existing Maven Project</b> e executar a aplicação a partir da classe TesteApplication.java</p>
<h5>Opção 2</h5>
<p>Executar utilizando o maven diretamente pela linha de comando, para isso, ir até a raiz da pasta do projeto e executar o comando "mvn spring-boot:run"</p>
<h3 style="margin-top: 30px;">Executando os testes de integração</h3>
<h5>Opção 1</h5>
<p>Na IDE de desenvolvimento, executar a classe disponível em src/test/java como JUnit Test.</p>
<h5>Opção 2</h5>
<p>Execur utilizando o maven diretamente pela linha de comando, para isso, ir até a raiz da pasta do projeto e executar o comando "mvn test".</p>

<h3 style="margin-top: 30px;">Profiles</h3>
<p>A aplicação está divida em dois profiles, um para prod e um para test.</p>
<p>O profile "prod" é o <i>default</i>, e utiliza o arquivo CSV presente em src/main/resources/csv/movielist.csv para realizar o carregamento inicial dos dados.<br/>
<b>O arquivo que possui os dados de inicialização sempre deverá estar neste diretório, com este nome</b></p>
<p>O profile "test" é habilitado na execução dos testes, e utiliza o arquivo CSV presente em src/main/resources/test/movielist-test.csv, com isso, é garantido que os testes serão executados sempre com os mesmos dados, não sendo afetado pela modificação do arquivo de produção.</p>

<h3 style="margin-top: 30px;">API</h3>
<p>Endpoint para obter os produtores que venceram dois prêmios no maior e menor intervalo:</p>
<p>GET - http://localhost:8080/producers/worst-producers</p>
