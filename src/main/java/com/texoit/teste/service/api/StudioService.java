package com.texoit.teste.service.api;

import com.texoit.teste.model.entity.Studio;

public interface StudioService {

	public Studio findByName(String name);
	
}
