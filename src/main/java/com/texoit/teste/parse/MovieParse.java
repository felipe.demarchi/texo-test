package com.texoit.teste.parse;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.texoit.teste.model.dto.CsvMovieDto;
import com.texoit.teste.model.entity.Movie;
import com.texoit.teste.model.entity.Producer;
import com.texoit.teste.model.entity.Studio;
import com.texoit.teste.service.api.ProducerService;
import com.texoit.teste.service.api.StudioService;

@Component
public class MovieParse implements Parse<CsvMovieDto, Movie>{

	private static final String YES = "yes";
	private static final String AND = " and ";
	private static final String COMMA_WITH_SPACE = ", ";
	private static final String COMMA = ",";
	
	@Autowired
	private StudioService studioService;
	
	@Autowired
	private ProducerService producerService;
	
	@Override
	public Movie parse(CsvMovieDto movieDto) {
		Movie movie = new Movie();
		movie.setYear(movieDto.getYear());
		movie.setTitle(movieDto.getTitle());
		movie.addStudios(this.getStudios(movieDto.getStudios()));
		movie.addProducers(this.getProducers(movieDto.getProducers()));
		movie.setWinner(MovieParse.YES.equalsIgnoreCase(movieDto.getWinner()));
		return movie;
	}
	
	private List<Studio> getStudios(String studios) {
		List<String> studiosAsString = this.splitText(studios);
		return studiosAsString.stream().map(this::getStudio).collect(Collectors.toList());
	}
	
	private Studio getStudio(String studioName) {
		Studio studio = this.studioService.findByName(studioName);
		if (studio == null) {
			return new Studio(studioName);
		}
		return studio;
	}
	
	private List<Producer> getProducers(String producers) {
		List<String> producersAsString = this.splitText(producers);
		return producersAsString.stream().map(this::getProducer).collect(Collectors.toList());
	}
	
	private Producer getProducer(String producerName) {
		Producer producer = this.producerService.findByName(producerName);
		if (producer == null) {
			return new Producer(producerName);
		}
		return producer;
	}
	
	private List<String> splitText(String text) {
		text = text.replaceAll(MovieParse.AND, MovieParse.COMMA_WITH_SPACE);
		String[] splitedValues = text.replaceAll(MovieParse.COMMA_WITH_SPACE, MovieParse.COMMA).split(MovieParse.COMMA);
		return Arrays.asList(splitedValues);
	}

}
