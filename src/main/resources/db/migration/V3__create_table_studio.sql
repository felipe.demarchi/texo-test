CREATE TABLE IF NOT EXISTS studio (
id INTEGER NOT NULL,
name VARCHAR(255) NOT NULL,
PRIMARY KEY (id)
);

CREATE SEQUENCE IF NOT EXISTS seq_studio START WITH 1 INCREMENT BY 1;