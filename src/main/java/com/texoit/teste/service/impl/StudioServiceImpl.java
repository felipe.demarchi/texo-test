package com.texoit.teste.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.texoit.teste.model.entity.Studio;
import com.texoit.teste.repository.StudioRepository;
import com.texoit.teste.service.api.StudioService;

@Service
public class StudioServiceImpl implements StudioService {

	@Autowired
	private StudioRepository studioRepository;
	
	@Override
	public Studio findByName(String name) {
		return this.studioRepository.findByName(name).orElse(null);
	}

}
