package com.texoit.teste.service.api;

import com.texoit.teste.model.dto.WorstProducersDto;
import com.texoit.teste.model.entity.Producer;

public interface ProducerService {

	public Producer findByName(String name);
	
	public WorstProducersDto findWortProducers();
	
}
