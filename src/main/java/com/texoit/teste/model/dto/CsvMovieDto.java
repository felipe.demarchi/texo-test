package com.texoit.teste.model.dto;

import java.io.Serializable;

import com.opencsv.bean.CsvBindByName;

public class CsvMovieDto implements Serializable {

	private static final long serialVersionUID = -460789738198373065L;

	@CsvBindByName(column = "year")
	private int year;
	
	@CsvBindByName(column = "title")
	private String title;
	
	@CsvBindByName(column = "studios")
	private String studios;
	
	@CsvBindByName(column = "producers")
	private String producers;
	
	@CsvBindByName(column = "winner")
	private String winner;

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStudios() {
		return studios;
	}

	public void setStudios(String studios) {
		this.studios = studios;
	}

	public String getProducers() {
		return producers;
	}

	public void setProducers(String producers) {
		this.producers = producers;
	}

	public String getWinner() {
		return winner;
	}

	public void setWinner(String winner) {
		this.winner = winner;
	}
	
}
