package com.texoit.teste.model.projection;

public interface ProducerWinsProjection {

	Long getIdProducer();
	
	Long getTotalWins();
	
}
