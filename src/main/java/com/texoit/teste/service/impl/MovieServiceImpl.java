package com.texoit.teste.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.texoit.teste.model.dto.CsvMovieDto;
import com.texoit.teste.model.entity.Movie;
import com.texoit.teste.parse.MovieParse;
import com.texoit.teste.repository.MovieRepository;
import com.texoit.teste.service.api.MovieService;

@Service
public class MovieServiceImpl implements MovieService {

	@Autowired
	private MovieRepository movieRepository;
	
	@Autowired
	private MovieParse movieParse;
	
	@Override
	@Transactional
	public void save(CsvMovieDto movieDto) {
		if (this.validate(movieDto)) {
			return;
		}
		Movie movie = this.movieParse.parse(movieDto);
		this.movieRepository.save(movie);
	}
	
	private boolean validate(CsvMovieDto movieDto) {
		return StringUtils.isEmpty(movieDto.getTitle())
				|| StringUtils.isEmpty(movieDto.getProducers())
				|| StringUtils.isEmpty(movieDto.getStudios())
				|| movieDto.getYear() <= 0;
				
	}
	
}
