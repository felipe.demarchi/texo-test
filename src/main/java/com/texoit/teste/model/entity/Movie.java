package com.texoit.teste.model.entity;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

@Entity
@Table(name = "movie")
public class Movie {
	
	@Id
	@Column(name = "id")
	@SequenceGenerator(name = "seq_movie", sequenceName = "seq_movie", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_movie")
	private Long id;
	
	@Column(name = "movie_year", nullable = false)
	private Integer year;
	
	@Column(name = "title")
	private String title;
	
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "studiomovie",
			joinColumns = {@JoinColumn(referencedColumnName = "id", name = "idmovie")},
			inverseJoinColumns = {@JoinColumn(referencedColumnName = "id", name = "idstudio")})
	private List<Studio> studios = new ArrayList<Studio>();
	
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "producermovie",
			joinColumns = {@JoinColumn(referencedColumnName = "id", name = "idmovie")},
			inverseJoinColumns = {@JoinColumn(referencedColumnName = "id", name = "idproducer")})
	private List<Producer> producers = new ArrayList<Producer>();
	
	@Column(name = "winner")
	private boolean winner;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Studio> getStudios() {
		return studios;
	}

	public void addStudios(List<Studio> studios) {
		this.studios.addAll(studios);
	}

	public List<Producer> getProducers() {
		return producers;
	}

	public void addProducers(List<Producer> producers) {
		this.producers.addAll(producers);
	}

	public boolean isWinner() {
		return winner;
	}

	public void setWinner(boolean winner) {
		this.winner = winner;
	}

}
