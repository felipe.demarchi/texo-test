CREATE TABLE IF NOT EXISTS studiomovie (
idstudio int not null,
idmovie int not null,
foreign key (idstudio) references studio (id),
foreign key (idmovie) references movie (id),
PRIMARY KEY (idstudio, idmovie)
);