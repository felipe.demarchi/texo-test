package com.texoit.teste.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.texoit.teste.model.dto.WorstProducersDto;
import com.texoit.teste.service.api.ProducerService;

@RestController
@RequestMapping("/producers")
public class ProducerController {

	@Autowired
	private ProducerService producerService;
	
	@GetMapping("/worst-producers")
	public ResponseEntity<WorstProducersDto> getWorstProducers() {
		WorstProducersDto worstProducersDto = this.producerService.findWortProducers();
		return new ResponseEntity<WorstProducersDto>(worstProducersDto, HttpStatus.OK);
	}
	
}
