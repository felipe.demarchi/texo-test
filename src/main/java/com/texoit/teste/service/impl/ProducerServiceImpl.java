package com.texoit.teste.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.texoit.teste.model.dto.WorstProducerInfoDto;
import com.texoit.teste.model.dto.WorstProducersDto;
import com.texoit.teste.model.entity.Producer;
import com.texoit.teste.model.projection.MultiProducersInfoProjection;
import com.texoit.teste.model.projection.ProducerWinsProjection;
import com.texoit.teste.repository.ProducerRepository;
import com.texoit.teste.service.api.ProducerService;

@Service
public class ProducerServiceImpl implements ProducerService {

	@Autowired
	private ProducerRepository producerRepository;
	
	@Override
	public Producer findByName(String name) {
		return this.producerRepository.findByName(name).orElse(null);
	}

	@Override
	public WorstProducersDto findWortProducers() {
		List<ProducerWinsProjection> producerWinsDto = this.producerRepository.findAllProducersWithAwards();
		List<Long> idsMultiAwardProducers = this.getMultiAwardsProducersIds(producerWinsDto);
		List<MultiProducersInfoProjection> producers = this.producerRepository.findProducersAwardsByIdProducer(idsMultiAwardProducers);
		
		List<WorstProducerInfoDto> allWorstProducers = generateWorstProducerInfoDtoForAllProducers(idsMultiAwardProducers, producers);
		
		return this.generateWorstProducerDto(allWorstProducers);
	}
	
	private WorstProducersDto generateWorstProducerDto(List<WorstProducerInfoDto> allWorstProducers) {
		int minInterval = allWorstProducers.stream().mapToInt(worstProducerInfo -> worstProducerInfo.getInterval()).min().orElse(0);
		int maxInterval = allWorstProducers.stream().mapToInt(worstProducerInfo -> worstProducerInfo.getInterval()).max().orElse(0);
		
		List<WorstProducerInfoDto> min = allWorstProducers.stream().filter(worstProducerInfo -> worstProducerInfo.getInterval() == minInterval).toList();
		List<WorstProducerInfoDto> max = allWorstProducers.stream().filter(worstProducerInfo -> worstProducerInfo.getInterval() == maxInterval).toList();
		
		return new WorstProducersDto(min, max);
	}
	
	private List<Long> getMultiAwardsProducersIds(List<ProducerWinsProjection> producerWinsDto) {
		return producerWinsDto
				.stream()
				.filter(producer -> producer.getTotalWins() > 1)
				.map(producer -> producer.getIdProducer())
				.collect(Collectors.toList());
	}
	
	private List<WorstProducerInfoDto> generateWorstProducerInfoDtoForAllProducers(List<Long> idsMultiAwardProducers, List<MultiProducersInfoProjection> producers) {
		List<WorstProducerInfoDto> intervals = new ArrayList<WorstProducerInfoDto>();
		idsMultiAwardProducers.forEach(idProducer -> {
			List<MultiProducersInfoProjection> filteredProducers = producers.stream().filter(producer -> producer.getProducerId().equals(idProducer)).toList();
			if (filteredProducers.size() < 2) {
				return;
			}
			intervals.addAll(this.calculateIntervals(filteredProducers));
		});
		
		return intervals;
	}
	
	private List<WorstProducerInfoDto> calculateIntervals(List<MultiProducersInfoProjection> filteredProducers) {
		List<WorstProducerInfoDto> intervals = new ArrayList<WorstProducerInfoDto>();
		for (int i = 1; i < filteredProducers.size(); i++) {
			WorstProducerInfoDto worstProducerInfoDto = new WorstProducerInfoDto();
			MultiProducersInfoProjection firstMovie = filteredProducers.get(i-1);
			MultiProducersInfoProjection secondMovie = filteredProducers.get(i);
			
			worstProducerInfoDto.setProducer(firstMovie.getProducerName());
			worstProducerInfoDto.setInterval(secondMovie.getYear() - firstMovie.getYear());
			worstProducerInfoDto.setPreviousWin(firstMovie.getYear());
			worstProducerInfoDto.setFollowingWin(secondMovie.getYear());
			
			intervals.add(worstProducerInfoDto);
		}
		return intervals;
	}
	
		
	

}
