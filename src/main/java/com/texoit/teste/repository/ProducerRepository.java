package com.texoit.teste.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.texoit.teste.model.entity.Producer;
import com.texoit.teste.model.projection.MultiProducersInfoProjection;
import com.texoit.teste.model.projection.ProducerWinsProjection;

@Repository
public interface ProducerRepository extends JpaRepository<Producer, Long>{

	Optional<Producer> findByName(String name);
	
	@Query("SELECT p.id as idProducer, count(*) as totalWins FROM Movie m "
			+ "INNER JOIN m.producers p "
			+ "WHERE m.winner = true "
			+ "GROUP BY p.id")
	List<ProducerWinsProjection> findAllProducersWithAwards();
	
	@Query("SELECT p.id as producerId, m.year as year, p.name as producerName "
			+ "FROM Movie m "
			+ "INNER JOIN m.producers p "
			+ "WHERE m.winner = true "
			+ "AND p.id IN (:idsProducers) "
			+ "ORDER BY p.id, m.year")
	List<MultiProducersInfoProjection> findProducersAwardsByIdProducer(@Param("idsProducers") List<Long> idsProducers);
	
}
