package com.texoit.teste.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.texoit.teste.model.entity.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

}
