CREATE TABLE IF NOT EXISTS producermovie (
idproducer int not null,
idmovie int not null,
foreign key (idproducer) references producer (id),
foreign key (idmovie) references movie (id),
PRIMARY KEY (idproducer, idmovie)
);