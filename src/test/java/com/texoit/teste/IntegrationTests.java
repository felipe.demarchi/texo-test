package com.texoit.teste;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import com.texoit.teste.model.dto.WorstProducerInfoDto;
import com.texoit.teste.model.dto.WorstProducersDto;
import com.texoit.teste.model.entity.Movie;
import com.texoit.teste.model.entity.Producer;
import com.texoit.teste.model.entity.Studio;
import com.texoit.teste.repository.MovieRepository;
import com.texoit.teste.repository.ProducerRepository;
import com.texoit.teste.repository.StudioRepository;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class IntegrationTests {
	
	private static final String MIN_PRODUCER_NAME = "Joel Silver";
	private static final int MIN_INTERVAL = 1;
	private static final int MIN_PREVIOUS_YEAR = 1990;
	private static final int MIN_FOLLOWING_YEAR = 1991;
	private static final String MAX_PRODUCER_NAME = "Matthew Vaughn";
	private static final int MAX_INTERVAL = 13;
	private static final int MAX_PREVIOUS_YEAR = 2002;
	private static final int MAX_FOLLOWING_YEAR = 2015;
	private static final int TOTAL_PRODUCERS_BY_ATTRIBUTE = 1;
	private static final int TOTAL_MOVIES_IMPORTED = 206;
	private static final int TOTAL_STUDIOS_IMPORTED = 59;
	private static final int TOTAL_PRODUCERS_IMPORTED = 360;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private MovieRepository movieRepository;
	
	@Autowired
	private StudioRepository studioRespository;
	
	@Autowired
	private ProducerRepository producerRepository;

	@Test
	public void csvImportOnStartTest() {
		List<Movie> movies = this.movieRepository.findAll();
		assertEquals(IntegrationTests.TOTAL_MOVIES_IMPORTED, movies.size());
		List<Studio> studios = this.studioRespository.findAll();
		assertEquals(IntegrationTests.TOTAL_STUDIOS_IMPORTED, studios.size());
		List<Producer> producers = this.producerRepository.findAll();
		assertEquals(IntegrationTests.TOTAL_PRODUCERS_IMPORTED, producers.size());
	}
	
	@Test
	public void getWorstProducersTest() {
		ResponseEntity<WorstProducersDto> response = restTemplate.getForEntity("/producers/worst-producers", WorstProducersDto.class);
		WorstProducersDto worstProducersDto = response.getBody();
		assertNotNull(worstProducersDto);
		assertEquals(IntegrationTests.TOTAL_PRODUCERS_BY_ATTRIBUTE, worstProducersDto.getMin().size());
		assertEquals(IntegrationTests.TOTAL_PRODUCERS_BY_ATTRIBUTE, worstProducersDto.getMax().size());
		WorstProducerInfoDto maxProducerInfo = worstProducersDto.getMax().stream().findFirst().orElse(new WorstProducerInfoDto());
		this.validateMaxProducerInfo(maxProducerInfo);
		WorstProducerInfoDto minProducerInfo = worstProducersDto.getMin().stream().findFirst().orElse(new WorstProducerInfoDto());
		this.validateMinProducerInfo(minProducerInfo);
	}
	
	private void validateMaxProducerInfo(WorstProducerInfoDto producerInfo) {
		assertEquals(IntegrationTests.MAX_PRODUCER_NAME, producerInfo.getProducer());
		assertEquals(IntegrationTests.MAX_INTERVAL, producerInfo.getInterval());
		assertEquals(IntegrationTests.MAX_PREVIOUS_YEAR, producerInfo.getPreviousWin());
		assertEquals(IntegrationTests.MAX_FOLLOWING_YEAR, producerInfo.getFollowingWin());
	}
	
	private void validateMinProducerInfo(WorstProducerInfoDto producerInfo) {
		assertEquals(IntegrationTests.MIN_PRODUCER_NAME, producerInfo.getProducer());
		assertEquals(IntegrationTests.MIN_INTERVAL, producerInfo.getInterval());
		assertEquals(IntegrationTests.MIN_PREVIOUS_YEAR, producerInfo.getPreviousWin());
		assertEquals(IntegrationTests.MIN_FOLLOWING_YEAR, producerInfo.getFollowingWin());
	}
	
}
