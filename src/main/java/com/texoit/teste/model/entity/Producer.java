package com.texoit.teste.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

@Entity
@Table(name = "producer")
public class Producer {

	@Id
	@Column(name = "id")
	@SequenceGenerator(name = "seq_producer", sequenceName = "seq_producer", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_producer")
	private Long id;
		
	@Column(name = "name")
	private String name;
	
	public Producer() {}
	
	public Producer(String name) {
		this.name = name;
	}
	
	public Producer(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
