package com.texoit.teste.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class WorstProducersDto implements Serializable {

	private static final long serialVersionUID = 7403656602317277228L;
	
	private List<WorstProducerInfoDto> min = new ArrayList<WorstProducerInfoDto>();
	
	private List<WorstProducerInfoDto> max = new ArrayList<WorstProducerInfoDto>();
	
	public WorstProducersDto() {}

	public WorstProducersDto(List<WorstProducerInfoDto> min, List<WorstProducerInfoDto> max) {
		this.min = min;
		this.max = max;
	}

	public List<WorstProducerInfoDto> getMin() {
		return min;
	}

	public void setMin(List<WorstProducerInfoDto> min) {
		this.min = min;
	}

	public List<WorstProducerInfoDto> getMax() {
		return max;
	}

	public void setMax(List<WorstProducerInfoDto> max) {
		this.max = max;
	}

}
