package com.texoit.teste.service.api;

import com.texoit.teste.model.dto.CsvMovieDto;

public interface MovieService {

	public void save(CsvMovieDto movieDto);
	
}
